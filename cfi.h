#ifndef CFI_H
#define CFI_H

#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

long get_file_size(FILE* f);

int validate_directory(char  file_path[]);

int validate_file(char file_path[]);

int read_bin_file(char file_path[], char** bytes);

int append_bin_file(char file_path[], int num_bytes, char* bytes);

int append_bin_file_unsigned(char file_path[], int num_bytes, unsigned char* bytes);

int append_bin_file_u_int16_t(char file_path[], u_int16_t num);

int read_u_int16_t(char file_path[], u_int16_t* num, int byte_offset);

int append_bin_file_int(char file_path[], int num);

int read_int(char file_path[], int* num, int byte_offset);

int clear_file(char file_path[]);

int create_file(char file_path[]);

#endif

