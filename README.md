# C File Interface

Whenever I make a project that has to read, write, and delete files. I always write the same functions over and over again using fread, fwrite, etc. So instead I will just include this library from now on to handle most file interactions. 

This repo is in Alpha, not for use. When the library is tested and functionally at a point that I personally would use it. I will put it into Beta. 