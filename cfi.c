#include <stdint.h>
#include <stdlib.h>

#include <stdio.h>
#include <string.h>
#include <endian.h>
#include <inttypes.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <dirent.h>
#include <errno.h>

#include "cfi.h"

/*
 * The format for return conditions in this library as follows
 *   >=1  | Function operation was successful with integer being 
 *	    	the return condition being the funtion return, or the 
 *	    	return code
 *   0    | The function completed successfuly with no return condition
 *   <=-1 | the funciton could not be completed, returning the error code
 * 	  		with -1 being the default
 */

/*
 * Returns the lenth of the file in bytes. 
 * 
 * Return conditions:
 *  >0  | The file is valid, returns the number of bytes
 *  -1  | The file is NULL
 */
long get_file_size(FILE* f)
{
	if(f == NULL)
	{
		return -1;
	}
	long lSize;
	fseek (f , 0 , SEEK_END);
	lSize = ftell (f);
	rewind (f);
	
	return lSize;
}

/*
 * Not for internal use.
 *
 * Return conditions:
 *    0 | The folder is valid
 *   -1 | The folder does not exist
 *   -2 | The opening of the folder failed
 */
int validate_file(char file_path[])
{
	FILE *f;
	
	if(!(f = fopen(file_path, "rb")))
	{
		return -1;
	}

	fclose(f);
	return 0;
}

/*
 * Return conditions:
 *   0  | The folder is valid
 *   -1 | The folder does not exist
 *   -2 | The opening of the folder failed
 */
int validate_directory(char file_path[])
{

	int i, j;
	for(i = strlen(file_path) - 1; file_path[i] != '/' && i != 0; i--);
	char folder_path [++i];
	for(j = 0; j < i; j++) folder_path[j] = file_path[j];
	folder_path[i] = '\0';

	DIR* dir = opendir(folder_path);
	if (dir) 
	{
		closedir(dir);
		/* Directory does exist. */
		return 0;	
	}
	 else if (ENOENT == errno) 
	{
		/* Directory does not exist. */
		return -1;
	}
	else 
	{
		/* opendir() failed for some other reason. */
		return -2;
	}
}

/*
 * Return conditions:
 *   >0 | The file is valid, returning the number of bytes
 *   -1 | The file does not exist
 *   -2 | There was an error reading the file
 */
int read_bin_file(char file_path[], char** bytes)
{
	FILE *f;
	long f_size;
	size_t error;

	//Check if the file can be opened	
	if(!(f = fopen ( file_path , "rb" ))){
		/* Could not open file. */
		return -1;
	}
	//Get the size of the file
	fseek (f , 0 , SEEK_END);
	f_size = ftell (f);
	rewind (f);

	//Check the return arrayint validate_file(char file_path[])
	if(bytes == NULL)
	{
		fclose(f);
		return f_size;
	}

	*bytes = (char*)malloc(sizeof(char) * f_size);
	
	error = fread (*bytes,1,f_size,f);
	
	fclose(f);
	if(error != f_size)
	{
		return -2;
	}
	
	return f_size;
}

/*
 * Return conditions:
 *   0  | The bytes were successfully appended
 *   -1 | The file does not exist
 */
int append_bin_file(char file_path[], int num_bytes, char* bytes)
{
	FILE *f;

	if (!(f = fopen(file_path, "ab")))
	{
		return -1;
	}
	
	fwrite (bytes , 1,num_bytes,f);
	
	fclose (f);
	
	return 0;
}

/*
 * Return conditions:
 *   0  | The bytes were successfully appended
 *   -1 | The file does not exist
 */
int append_bin_file_unsigned(char file_path[], int num_bytes, unsigned char* bytes)
{
	FILE *f;
	
	f = fopen(file_path, "ab+");
	if (!f)
	{
		return -1;
	}
	
	f = fopen(file_path, "ab");	
	if (!f)
	{
		return -1;
	}
	
	fwrite (bytes , 1,num_bytes,f);
	
	fclose (f);
	
	return 0;
}

/*
 * Return conditions:
 *   0  | The bytes were successfully appended
 *   -1 | The file does not exist
 */
int append_bin_file_u_int16_t(char file_path[], u_int16_t num)
{
	u_int16_t temp = htobe16(num);
	
	FILE *f;
	
	f = fopen(file_path, "ab+");
	
	f = fopen(file_path, "ab");	
	if (!f)
	{
		return -1;
	}
	fwrite (&temp, sizeof(u_int16_t),1, f);
	fclose (f);
	
	return 0;
}

/*
 * Return conditions:
 *   0  | The integer was successfully read
 *   -1 | The file does not exist
 *   -2 | The integer was not successfully read
 */
int read_int(char file_path[], int* num, int byte_offset)
{
	FILE *f;
	
	if(!(f = fopen ( file_path , "rb" ))){
		/* Could not open file. */
		return -1;
	}
	
	fseek(f, byte_offset, SEEK_SET);
	int temp;
	
	if(fread(&temp, sizeof(int), 1, f) == 0)
	{	
		fclose(f);
		return -2;
	}
	
	*num = htobe32(temp);
	
	fclose(f);	
	return 0;
}

/*
 * Return conditions:
 *   0  | The uint16 was successfully read
 *   -1 | The file does not exist
 *   -2 | The uint16 was not successfully read
 */
int read_u_int16_t(char file_path[], u_int16_t* num, int byte_offset)
{
	FILE *f;

	if(!(f = fopen ( file_path , "rb" ))){
		/* Could not open file. */
		return -1;
	}
	
	fseek(f, byte_offset, SEEK_SET);
	u_int16_t temp;
	
	if(fread(&temp, sizeof(u_int16_t), 1, f) == 0)
	{
		fclose(f);
		return -2;
	}
	
	*num = htobe16(temp);
	
	fclose(f);
	return 0;
}

/*
 * Return conditions:
 *   0  | The integer was successfully appended
 *   -1 | The file does not exist
 */
int append_bin_file_int(char file_path[], int num)
{
	int temp = htobe32(num);
	
	FILE *f;
	
	f = fopen(file_path, "ab+");
	
	f = fopen(file_path, "ab");	
	if (!f)
	{
		return -1;
	}
	fwrite (&temp, sizeof(int),1, f);
	fclose (f);
	
	return 0;
}

/*
 * Return conditions:
 *   0  | File successfully cleared
 *   -1 | The file does not exist
 */
int clear_file(char file_path[])
{
	FILE *f;
	if(!(f = fopen ( file_path , "wb" ))){
		/* Could not open file. */
		return -1;
	}
	
	fclose(f);
	
	return 0;
}

/*
 * Return conditions:
 *   0  | File successfully created
 *   -1 | The file path does not exist
 */
int create_file(char file_path[])
{
	FILE *f;
	if(!(f = fopen ( file_path , "ab+" ))){
		/* Could not open file. */
		return -1;
	}	
	
	fclose(f);
	return 0;
}
