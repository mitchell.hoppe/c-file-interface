# Specify extensions of files to delete when cleaning
CLEANEXTS   = o a 

# Specify the target file and the install directory
OUTPUTFILE  = libcfileinterface.a
INSTALLDIR  = ../binaries

# Default target
.PHONY: all
all: $(OUTPUTFILE)

test: cfi.o test.o
	rm -f test
	gcc -g -Wall -Werror -pedantic test.c cfi.h cfi.c -o test
	$(if $(filter foo,bar),@echo match is broken,@echo match works)

# Build libcfileinterface.a from cfi.o
$(OUTPUTFILE): cfi.o
	ar ru $@ $^
	ranlib $@

# No rule to build cfi.o  from .cpp 
# files is required; this is handled by make's database of
# implicit rules

.PHONY: install
install:
	mkdir -p $(INSTALLDIR)
	cp -p $(OUTPUTFILE) $(INSTALLDIR)

.PHONY: clean 
clean:
	for file in $(CLEANEXTS); do rm -f *.$$file; done
	rm -f test

# Indicate dependencies of .ccp files on .hpp files
cfi.o: cfi.h
