#include "./cfi.h"
#include "greatest.h"
#include "stdio.h"

TEST test_validate_folder(void) 
{
	char* valid_folder   = "./";
	char* invalid_folder = "./somefolder/folder";

	int valid_pass;
	int invalid_pass;
	
	valid_pass   = validate_directory(valid_folder);
	invalid_pass = validate_directory(invalid_folder);
	invalid_pass = (invalid_pass < 0 ? -1 : invalid_pass);
	
	ASSERT_EQ(0, valid_pass);
	ASSERT_EQ(-1, invalid_pass);

	PASS();
}

TEST test_create_valid_file(void) {
	char file_path[] = "./file";
	int create_pass;
	int validf_pass;
	int delete_pass;
	
	create_pass = create_file(file_path);
	printf("\n%d\n", create_file(file_path));
	validf_pass = validate_file(file_path);
	delete_pass = remove(file_path);
	
	ASSERT_EQ(0, create_pass);
	ASSERT_EQ(0, validf_pass);
	ASSERT_EQ(0, delete_pass);

	PASS();
}

TEST test_create_invalid_file(void) {
	char file_path[] = "./somefolder/file";
	
	int create_pass;
	int validf_pass;
	int delete_pass;
	
	create_pass = create_file(file_path);
	validf_pass = validate_file(file_path);
	delete_pass = remove(file_path);
	
	ASSERT_EQ(-1, create_pass);
	ASSERT_EQ(-1, validf_pass);
	ASSERT_EQ(-1, delete_pass);

	PASS();
}

TEST test_read_bin_file(void) 
{
	char file_path[] = "./bin_file";
	char bytes[] = {0,1,2,3,4,5,6,7,8,9};
	char *ptr = bytes;

	int      create_pass;
	int      validf_pass;
	int  empty_read_pass;
	int      append_pass;
	int filled_read_pass;
	int      delete_pass;

	create_pass = create_file(file_path);
	validf_pass = validate_file(file_path);
	empty_read_pass = (read_bin_file(file_path, NULL ) == 0 ? 0 : 1);
	append_pass = append_bin_file(file_path, 10, &ptr[0]);
	filled_read_pass = (read_bin_file(file_path, NULL ) == 10 ? 0 : 1);
	delete_pass = remove(file_path);
	
	ASSERT_EQ(0, create_pass);
	ASSERT_EQ(0, validf_pass);
	ASSERT_EQ(0, empty_read_pass);
	ASSERT_EQ(0, append_pass);
	ASSERT_EQ(0, filled_read_pass);
	ASSERT_EQ(0, delete_pass);

	PASS();
}

SUITE(test_suite) {
	(void)test_create_invalid_file;
	(void)test_create_valid_file;
	RUN_TEST(test_create_valid_file);
	RUN_TEST(test_create_invalid_file);
	RUN_TEST(test_validate_folder);
	RUN_TEST(test_read_bin_file);
}

GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
	GREATEST_MAIN_BEGIN();

	RUN_SUITE(test_suite);

	GREATEST_MAIN_END();
}

